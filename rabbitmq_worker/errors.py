class InvalidChannel(Exception):
    pass


class UnrecoverableWorkerError(Exception):
    pass